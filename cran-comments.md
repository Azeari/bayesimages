Fixed all http: URLs by changing to https: and also using new DOI syntax

## Test environments

* `devtools::check_win_release` OK, see status at
  + <https://win-builder.r-project.org/ODnQ0xjcpf02/00check.log>
* `rhub::check_for_cran`, see status at
   + `ubuntu-gcc-release` <https://builder.r-hub.io/status/bayesImageS_0.6-1.tar.gz-d3de9c73f91a430ea50918a8b742e721>
   + `debian-gcc-release` <https://builder.r-hub.io/status/bayesImageS_0.6-1.tar.gz-4374f92d42024e77b7f2defe941fe57e>
   + `solaris-x86-patched` <https://builder.r-hub.io/status/bayesImageS_0.6-1.tar.gz-9b991377a03345e7a3df0e2193b0b29b>
   + `macos-highsierra-release-cran` <https://builder.r-hub.io/status/bayesImageS_0.6-1.tar.gz-803450fe6ed34cc2aac6a9bd8a5b9f73>
* local macOS 10.15.7 (Catalina) install, R version 4.0.5 (2021-03-31) from CRAN

## R CMD check results
There were no ERRORs nor WARNings. 

There was 1 NOTE:

*   installed size is  6.0Mb. sub-directories of 1Mb or more:
    - data   1.6Mb
    - libs   3.7Mb

## Downstream dependencies
There are currently no downstream dependencies for this package.
